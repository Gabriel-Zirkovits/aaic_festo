from sklearn import metrics
import numpy as np
import re

def multilabel_metrics(y_true, y_pred):
    print(f'Exact Match Ratio: {metrics.accuracy_score(y_true, y_pred, normalize=True, sample_weight=None)}')
    #One trivial way around would just to ignore partially correct (consider them incorrect) and extend the accuracy
    # used in single label case for multi-label prediction.

    print(f'Hamming loss: {metrics.hamming_loss(y_true, y_pred)}')
    # The Hamming loss is the fraction of labels that are incorrectly predicted.

    print(f"I/O Loss: {np.any(y_true != y_pred, axis=1).mean()}")
    #This metric is basically known as 1−Exact Match Ratio, where we calculate proportions of instances whose
    # actual value is not equal to predicted value.

    print(f"Label ranking average precision: {metrics.label_ranking_average_precision_score(y_true, y_pred)}")
    # The obtained score is always strictly greater than 0 and the best value is 1.


def extract_email_address(text):
    matches = re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", text)

    for match in matches: # Remove festo mail adresses
        if re.search("festo", match):
            matches.remove(match)
    return matches

def extract_person_names(text):
    # load spacy nlp before!
    # text = spacy doc type
    return [entity.text for entity in text.ents if entity.label_ == "PERSON"]

